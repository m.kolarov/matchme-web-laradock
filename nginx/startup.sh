#!/bin/bash

if [ ! -f /etc/nginx/ssl/matchme.local.crt ]; then
    openssl genrsa -out "/etc/nginx/ssl/matchme.local.key" 2048
    openssl req -new -key "/etc/nginx/ssl/matchme.local.key" -out "/etc/nginx/ssl/matchme.local.csr"
    openssl x509 -req -days 365 -in "/etc/nginx/ssl/matchme.local.csr" -signkey "/etc/nginx/ssl/matchme.local.key" -out "/etc/nginx/ssl/matchme.local.crt" -sha256 -extfile v3.ext
fi

nginx
